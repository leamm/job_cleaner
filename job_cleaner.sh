#!/bin/bash

JENKINS_WORKSPACE_DIR='/var/www/jenkins/workspace/'

job_name=$1
job_num=`echo $1 | egrep -o '[0-9]+'`

if [ -z "$job_num" ]; then
    echo "Job name does not contain a number"
    exit
fi

echo "Cleaning... job $job_name"
echo "Job num: $job_num"

# kill tmux session
echo "Kill tmux sessions..."
tmux_sess=`tmux ls | grep -oP "^(GS|EZ|LS)$job_num:" | cut -d: -f1`
if [ -n "$tmux_sess" ]; then
    for sess in $tmux_sess; do
        echo "Kill $sess"
        tmux kill-session -t $sess
    done
else
    echo "Session not found"
fi

# drop mysql databases
echo "Drop mysql databases..."
mysql_dbs=`mysql -uroot -e 'SHOW DATABASES;' | grep "$job_num[^0-9]"`
if [ -n "$mysql_dbs" ]; then
    for db in $mysql_dbs; do
        echo "Drop $db"
        mysql -uroot -e "DROP DATABASE $db;"
    done
else
    echo "Mysql database not found"
fi

# rm workspace dir
echo "Remove workspace dirs..."
workspace_dirs=`find $JENKINS_WORKSPACE_DIR -maxdepth 1 -type d -regex ".*$job_num[^0-9].*"`
if [ -n "$workspace_dirs" ]; then
    for dir in $workspace_dirs; do
        echo "Remove $dir"
        rm -rf $dir
    done
else
    echo "Workspace dirs not found"
fi

# Clean redis storages
echo "Clean redis storages"
redis_dbs=`redis-cli INFO keyspace | cut -d: -f1 | grep -oP "(?<=db)$job_num\d$"`
if [ -n "$redis_dbs" ]; then
    for db in $redis_dbs; do
        echo "Clean $db"
        redis-cli -n $db FLUSHDB
    done
else
    echo "Redis database not found"
fi


echo 'Done!'
